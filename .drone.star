def main(ctx):
  pipeline = {
    "kind": "pipeline",
    "name": "dotnet",
    "steps": []
  }

  build = {
    "name": "build",
    "image": "mcr.microsoft.com/dotnet/sdk:7.0",
    "commands": [
      "cd Sources",
      "dotnet restore OpenLibraryWS_Wrapper.sln",
      "dotnet build OpenLibraryWS_Wrapper.sln -c Release --no-restore",
      "dotnet publish OpenLibraryWS_Wrapper.sln -c Release --no-restore -o $CI_PROJECT_DIR/build/release"
    ]
  }

  tests = {
    "name": "tests",
    "image": "mcr.microsoft.com/dotnet/sdk:7.0",
    "commands": [
      "cd Sources/",
      "dotnet restore OpenLibraryWS_Wrapper.sln",
      "dotnet test OpenLibraryWS_Wrapper.sln --no-restore"
    ],
    "depends_on": ["build"]
  }

  hadolint = {
    "name": "hadolint",
    "image": "hadolint/hadolint:latest-debian",
    "commands": [
      "cd Sources",
      "hadolint --ignore DL3059 Dockerfile"
    ],
    "depends_on": ["build"]
  }

  code_inspection = {
    "name": "code-inspection",
    "image": "hub.codefirst.iut.uca.fr/marc.chevaldonne/codefirst-dronesonarplugin-dotnet7",
    "secrets": ["SECRET_SONAR_LOGIN"],
    "settings": {
      "sonar_host": "https://codefirst.iut.uca.fr/sonar/",
      "sonar_token": {
        "from_secret": "SECRET_SONAR_LOGIN"
      }
    },
    "commands": [
      "cd Sources/",
      "dotnet restore OpenLibraryWS_Wrapper.sln",
      "dotnet sonarscanner begin /k:Dotnet_Dorian_HODIN_CodeFirst /d:sonar.host.url=$${PLUGIN_SONAR_HOST} /d:sonar.coverageReportPaths='coveragereport/SonarQube.xml' /d:sonar.coverage.exclusions='Tests/**' /d:sonar.login=$${PLUGIN_SONAR_TOKEN}",
      "dotnet build OpenLibraryWS_Wrapper.sln -c Release --no-restore",
      "dotnet test OpenLibraryWS_Wrapper.sln --logger trx --no-restore /p:CollectCoverage=true /p:CoverletOutputFormat=cobertura --collect 'XPlat Code Coverage'",
      "reportgenerator -reports:'**/coverage.cobertura.xml' -reporttypes:SonarQube -targetdir:'coveragereport'",
      "dotnet publish OpenLibraryWS_Wrapper.sln -c Release --no-restore -o CI_PROJECT_DIR/build/release",
      "dotnet sonarscanner end /d:sonar.login=$${PLUGIN_SONAR_TOKEN}"
    ],
    "depends_on": ["build", "tests"]
  }
  generate_and_deploy_docs = {
    "name": "generate-and-deploy-docs",
    "image": "hub.codefirst.iut.uca.fr/thomas.bellembois/codefirst-docdeployer",
    "failure": "ignore",
    "volumes": [
      {
        "name": "docs",
        "path": "/docs"
      }
    ],
    "commands": [
      "/entrypoint.sh"
    ],
    "depends_on": ["build", "tests"]
  }

  generate_swashbuckle = {
    "name": "generate-swashbuckle",
    "image": "mcr.microsoft.com/dotnet/sdk:7.0",
    "commands": [
      "cd Sources/OpenLibraryWrapper",
      "dotnet tool install --version 6.5.0 Swashbuckle.AspNetCore.Cli --tool-path /bin",
      "/bin/swagger tofile --output /drone/src/swagger.json bin/Release/net7.0/OpenLibraryWrapper.dll v1"
    ],
    "depends_on": ["build"]
  }

  deploy_swagger = {
    "name": "deploy-swagger",
    "image": "hub.codefirst.iut.uca.fr/maxime.batista/codefirst-docdeployer",
    "commands": [
      "/entrypoint.sh --type swagger --loc /drone/src/swagger.json"
    ],
    "depends_on": ["generate-swashbuckle"]
  }


  docker_build_and_push = {
    "name": "docker-build-and-push",
    "image": "plugins/docker",
    "settings": {
      "dockerfile": "Sources/Dockerfile",
      "context": "Sources/",
      "registry": "hub.codefirst.iut.uca.fr",
      "repo": "hub.codefirst.iut.uca.fr/dorian.hodin/openlibraryws_wrapper_dh",
      "username": {
        "from_secret": "SECRET_REGISTRY_USERNAME"
      },
      "password": {
        "from_secret": "SECRET_REGISTRY_PASSWORD"
      }
    },
    "depends_on": ["build", "tests"]
  }

  deploy_app = {
    "name": "deploy-app",
    "image": "hub.codefirst.iut.uca.fr/thomas.bellembois/codefirst-dockerproxy-clientdrone:latest",
    "environment": {
      "IMAGENAME": "hub.codefirst.iut.uca.fr/dorian.hodin/openlibraryws_wrapper_dh",
      "CONTAINERNAME": "app_dotnet_dh",
      "COMMAND": "create",
      "OVERWRITE": "true",
      "CODEFIRST_CLIENTDRONE_ENV_DB_DATABASE": "dhdb"
    },
    "depends_on": ["docker-build-and-push"]
  }

  deploy_container_mariadb = {
    "name": "deploy-container-mariadb",
    "image": "hub.codefirst.iut.uca.fr/thomas.bellembois/codefirst-dockerproxy-clientdrone:latest",
    "environment": {
      "IMAGENAME": "mariadb:10",
      "CONTAINERNAME": "dhdb",
      "COMMAND": "create",
      "OVERWRITE": "false",
      "PRIVATE": "true",
      "CODEFIRST_CLIENTDRONE_ENV_MARIADB_ROOT_PASSWORD": {
        "from_secret": "db_root_password"
      },
      "CODEFIRST_CLIENTDRONE_ENV_MARIADB_DATABASE": {
        "from_secret": "db_database"
      },
      "CODEFIRST_CLIENTDRONE_ENV_MARIADB_USER": {
        "from_secret": "db_user"
      },
      "CODEFIRST_CLIENTDRONE_ENV_MARIADB_PASSWORD": {
        "from_secret": "db_password"
      }
    },
    "depends_on": ["deploy-app"]

  }

  if ctx.build.branch == "master":
    pipeline["steps"].append(build)
    pipeline["steps"].append(tests)
    pipeline["steps"].append(hadolint)
    pipeline["steps"].append(code_inspection)
    pipeline["steps"].append(generate_and_deploy_docs)
    pipeline["steps"].append(generate_swashbuckle)
    pipeline["steps"].append(deploy_swagger)
    pipeline["steps"].append(docker_build_and_push)
    pipeline["steps"].append(deploy_app)
    pipeline["steps"].append(deploy_container_mariadb)

  if "[database]" in ctx.build.message:
    deploy_app["environment"]["CODEFIRST_CLIENTDRONE_ENV_DB_DATABASE"] = "dhdb"
    deploy_app["name"] = "database-deploy-container"
  elif "[wrapper]" in ctx.build.message:
    deploy_app["environment"]["CODEFIRST_CLIENTDRONE_ENV_DB_DATABASE"] = "dhAPI"
    deploy_app["name"] = "wrapper-deploy-container"
  elif "[stub]" in ctx.build.message:
    deploy_app["environment"]["CODEFIRST_CLIENTDRONE_ENV_DB_DATABASE"] = "dhstub"
    deploy_app["name"] = "stub-deploy-container"

  if ctx.build.branch != "master":

    pipeline["steps"].append(build)
    pipeline["steps"].append(tests)
    pipeline["steps"].append(hadolint)
    pipeline["steps"].append(code_inspection)
    pipeline["steps"].append(generate_and_deploy_docs)
    pipeline["steps"].append(generate_swashbuckle)
    pipeline["steps"].append(deploy_swagger)
    pipeline["steps"].append(docker_build_and_push)
    pipeline["steps"].append(deploy_app)
    pipeline["steps"].append(deploy_container_mariadb)


    if "[sonar]" not in ctx.build.message and "[ci_all]" not in ctx.build.message:
      new_steps = []
      for step in pipeline["steps"]:
        if step.get("name") != "code-inspection":
          new_steps.append(step)
      pipeline["steps"] = new_steps

    if "[doxygen]" not in ctx.build.message and "[swagger]" not in ctx.build.message:
      steps_to_remove = ["generate-and-deploy-docs", "generate-swashbuckle", "deploy-swagger"]
      new_steps = []
      for step in pipeline["steps"]:
        if step.get("name") not in steps_to_remove:
          new_steps.append(step)
      pipeline["steps"] = new_steps

    if "[cd]" in ctx.build.message:
        if "[database]" in ctx.build.message:
          deploy_app["environment"]["CODEFIRST_CLIENTDRONE_ENV_DB_DATABASE"] = "dhdb"
          deploy_app["name"] = "database-deploy-container"
        elif "[wrapper]" in ctx.build.message:
          deploy_app["environment"]["CODEFIRST_CLIENTDRONE_ENV_DB_DATABASE"] = "dhAPI"
          deploy_app["name"] = "wrapper-deploy-container"
        elif "[stub]" in ctx.build.message:
          deploy_app["environment"]["CODEFIRST_CLIENTDRONE_ENV_DB_DATABASE"] = "dhstub"
          deploy_app["name"] = "stub-deploy-container"
    else:
      steps_to_remove = ["docker-build-and-push", "deploy-container", "deploy-container-mariadb"]
      new_steps = []
      for step in pipeline["steps"]:
        if step.get("name") not in steps_to_remove:
          new_steps.append(step)
      pipeline["steps"] = new_steps


  if "ReadMe.md" in ctx.build.message or "[no_ci]" in ctx.build.message:
    pipeline["steps"] = []
    return pipeline

  if ".drone.star" in ctx.build.message:
    steps_to_remove = ["code-inspection", "generate-and-deploy-docs", "generate-swashbuckle", "deploy-swagger"]
    new_steps = []
    for step in pipeline["steps"]:
      if step.get("name") not in steps_to_remove:
        new_steps.append(step)
    pipeline["steps"] = new_steps

  if "[overwrite_db]" in ctx.build.message:
    deploy_app["name"] = "deploy-container-mariadb-overwrite"
    deploy_app["environment"]["OVERWRITE"] = "true"

  return pipeline

